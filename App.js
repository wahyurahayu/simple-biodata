import React, { Component } from 'react';
import { Text, View, TextInput, StyleSheet } from 'react-native';
import CustomButton from './button';

export class App extends Component {
  state = {
    name: '',
    tanggallahir: '',
    alamat: '',
    skill: '',
  };
  render() {
    return (
      <View style={styles.bungkus}>
        <Text style={styles.teks}> Biodata </Text>
        <TextInput onChangeText={text => this.setState({name: text})} />

        <Text style={styles.label}> Nama : {this.state.name}</Text>
        <TextInput style={styles.input} onChangeText={text => this.setState({nama: text})}/>

        <Text style={styles.label}> tanggal lahir : {this.state.tanggallahir}</Text>
        <TextInput style={styles.input} onChangeText={text => this.setState({tanggallahir: text})}/>

        <Text style={styles.label}> Alamat : {this.state.alamat}</Text>
        <TextInput style={styles.input} onChangeText={text => this.setState({alamat: text})} />

        <Text style={styles.label}> skill : {this.state.skill}</Text>
        <TextInput style={styles.input} onChangeText={text => this.setState({skill: text})} />

        <CustomButton
          label="Simpan" 
        />

      </View>
    );
  }
}

export default App;

// import React, {Component} from 'react';
// import {
//   Text,
//   View,
//   TextInput,
//   StyleSheet
// } from 'react-native';

// export class App extends Component {
//   state = {
//     name: '',
//     tanggallahir: '',
//     alamat: '',
//     skill: '',
//   };
//   render() {
//     return (
//       <View style={styles.bungkus}>
//         <Text style={styles.teks}> Biodata </Text>
//         <TextInput onChangeText={text => this.setState({name: text})} />

//         <Text style={styles.label}> Nama : {this.state.name}</Text>
//         <TextInput style={styles.input} onChangeText={text => this.setState({nama: text})}/>

//         <Text style={styles.label}> tanggal lahir : {this.state.tanggallahir}</Text>
//         <TextInput style={styles.input} onChangeText={text => this.setState({tanggallahir: text})}/>

//         <Text style={styles.label}> Alamat : {this.state.alamat}</Text>
//         <TextInput style={styles.input} onChangeText={text => this.setState({alamat: text})} />

//         <Text style={styles.label}> skill : {this.state.skill}</Text>
//         <TextInput style={styles.input} onChangeText={text => this.setState({skill: text})} />

//       </View>
//     );
//   }
// }

// export default App;

const styles = StyleSheet.create({
  input: {
    height: 40,
    marginHorizontal: 20,
    borderRadius: 50,
    backgroundColor: '#FFFFFF',
    elevation: 3,
    paddingHorizontal: 20,
  },
  teks:{
    fontSize: 30,
    color:'black',
    margin: 30,
    textAlign:'center',
    padding: 10,
  },
  button: {
    alignItems: 'center',
    backgroundColor: '#DDDDDD',
    padding: 10
  },
  label: {
    margin: 20,
    padding: 5,
  },
  title: {
    textAlign: 'center',
    marginVertical: 8,
  },
  fixToText: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  separator: {
    marginVertical: 8,
    borderBottomColor: '#737373',
    borderBottomWidth: StyleSheet.hairlineWidth,
  },
});


