import React from 'react';
import {View, Text, TouchableOpacity, StyleSheet} from 'react-native';

const CustomButton = props => {
  return (
    <TouchableOpacity onPress={props.onPress} style={styles.btn}>
      <Text style={styles.text}>{props.label}</Text>
    </TouchableOpacity>
  );
};

export default CustomButton;

const styles = StyleSheet.create({
  btn: {
    backgroundColor: 'green',
    padding: 15,
    borderRadius: 20,
    margin: 30,
    alignItems: 'center',
  },
  text: {
    color: 'white',
    fontSize: 16,
    fontWeight: 'bold',
  },
});



// import React from 'react';
// import { View, Text, StyleSheet, TouchableOpacity } from 'react-native';

// const customButton = (props) => {
//     return (
//     <View>
//       <TouchableOpacity onPress={props.onPress} style={Style.btn}>
//         <Text style={style.text}>{props.label}</Text>
//       </TouchableOpacity>
//     </View>
//     );
// }

// export default customButton;

// const style = StyleSheet.create ({
//     btn: {
//         backgroundColor: 'Black',
//         padding: 10,
//         borderRadius: '50',
//     },
//     text: {
//         color: 'white',
//     },
// });